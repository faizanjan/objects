const invert = require("../invert.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const result = invert(testObject);

if(result.length!==0) console.log(result);
else console.log ("Pass proper arguments")