const mapObjects = require("../mapObjects.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const callback = (val, key) => val + "Faizan";

const result = mapObjects(testObject,callback);

if(result.length!==0)console.log(result);
else console.log ("Pass proper arguments")