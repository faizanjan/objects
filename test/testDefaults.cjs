const defaults = require("../defaults.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const result = defaults( testObject);

if(result.length!==0) console.log(result);
else console.log ("Pass proper arguments")