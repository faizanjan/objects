const checkArg = require("./argumentCheck.cjs");

function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert    

    if (!checkArg(obj)) return [];

    let newObj = {};
    for (let key in obj){
        const newKey = obj[key]
        newObj[newKey]=key;
    }
    return newObj;
}

module.exports = invert