const checkArg = require("./argumentCheck.cjs");

function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys

    if (!checkArg(obj)) return [];
    let keyList = [];
    for (let key in obj){
        keyList.push(key);
    }
    return keyList
}

module.exports = keys;