const checkArg = require("./argumentCheck.cjs");

function mapObject(obj, callback) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    if (!checkArg(obj)) return [];
    let newObject = {};
    for (let key in obj){
        newObject[key]=callback(obj[key],key);
    }
    return newObject;
}

module.exports = mapObject