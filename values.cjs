const checkArg = require("./argumentCheck.cjs");

function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

    if (!checkArg(obj)) return [];
    let valueList = [];
    for (let value in obj){
        valueList.push(obj[value]);
    }
    return valueList;
}

module.exports = values