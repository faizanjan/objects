const checkArg = require("./argumentCheck.cjs");
const keys = require("./keys.cjs");

function defaults(object, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `object`.
    // http://underscorejs.org/#defaults   

    if (!checkArg(object)) return [];

    const objKeys = keys(object);
    for (let key in defs){
        if(!objKeys.includes(key)){
            object[key]=defs[key];
        }
    }
    return object;
}

module.exports = defaults