const checkArg = require("./argumentCheck.cjs");

function pairs(obj) {
    // Convert an object into a list of [key, key] pairs.
    // http://underscorejs.org/#pairs

    if (!checkArg(obj)) return [];
    let pairList = [];
    for (let key in obj){
        pairList.push([key,obj[key]]);
    }
    return pairList;
}

module.exports = pairs