
const checkArg = arg =>{
    if(!arg || typeof(arg)!=='object' || arg.length===0) return false;
    else return true;
}

module.exports = checkArg;